package edu.tadzhibaev;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractWebPage {

    protected final WebDriver webDriver;

    public AbstractWebPage(final WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    protected void waitForElementDisappears(By by) {
        WebDriverWait wait = new WebDriverWait(webDriver, 5L);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    protected void waitForElementIsVisible(By by) {
        WebDriverWait wait = new WebDriverWait(webDriver, 5L);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    protected void waitForElementIsVisible(WebElement webElement) {
        WebDriverWait wait = new WebDriverWait(webDriver, 5L);
        wait.until(ExpectedConditions.visibilityOf(webElement));
    }
}
