package edu.tadzhibaev.taskone;

import edu.tadzhibaev.taskone.common.utils.FileUtils;
import edu.tadzhibaev.taskone.common.utils.sorters.Ordering;

import java.io.IOException;
import java.net.URISyntaxException;

public class Runner {

    private static final String FILE_NAME = "numbers.txt";

    public static void main(String[] args) throws IOException, URISyntaxException {
        FileUtils.sortAndPrintResult(FILE_NAME, Ordering.NATURAL);
        FileUtils.sortAndPrintResult(FILE_NAME, Ordering.REVERSED);
    }
}
