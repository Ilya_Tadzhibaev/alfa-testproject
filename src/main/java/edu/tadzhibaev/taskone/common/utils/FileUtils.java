package edu.tadzhibaev.taskone.common.utils;

import edu.tadzhibaev.taskone.common.utils.loggers.ConsoleLogger;
import edu.tadzhibaev.taskone.common.utils.parsers.TextParser;
import edu.tadzhibaev.taskone.common.utils.sorters.CollectionSorter;
import edu.tadzhibaev.taskone.common.utils.sorters.Ordering;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {

    public FileUtils() {
        //to restrict new instance creation, it's utils class
    }

    public static void sortAndPrintResult(String resourceFileName, Ordering ordering) throws IOException, URISyntaxException {
        List<Integer> parsedIntegers = TextParser.parseFile(resourceFileName);
        List<Integer> sortedIntegers = new ArrayList<>();
        switch (ordering) {
            case NATURAL:
                sortedIntegers = CollectionSorter.sortDescent(parsedIntegers);
                break;
            case REVERSED:
                sortedIntegers = CollectionSorter.sortAscent(parsedIntegers);
                break;
        }
        System.out.println("Ordering: " + ordering.name());
        ConsoleLogger.printCollection(sortedIntegers);
    }
}
