package edu.tadzhibaev.taskone.common.utils.loggers;

import java.util.List;

public class ConsoleLogger {

    private ConsoleLogger() {
        //to restrict new instance creation, it's utils class
    }

    public static void printCollection(List<Integer> integerList) {
        System.out.println("Printing task started");
        integerList.forEach(System.out::println);
        System.out.println("Printing task finished");
    }
}
