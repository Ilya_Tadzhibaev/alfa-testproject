package edu.tadzhibaev.taskone.common.utils.parsers;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TextParser {

    private TextParser() {
        //to restrict new instance creation, it's utils class
    }

    public static List<Integer> parseFile(String resourceFileName) throws IOException, URISyntaxException {
        List<Integer> parsedStringNumbers;

        try (BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(ClassLoader.getSystemResource(resourceFileName).toURI()))) {
            String[] split = bufferedReader
                    .readLine()
                    .replaceAll(" ", "")
                    .split(",");

            parsedStringNumbers = Arrays.stream(split).map(Integer::parseInt).collect(Collectors.toList());
        }
        return parsedStringNumbers;
    }

}
