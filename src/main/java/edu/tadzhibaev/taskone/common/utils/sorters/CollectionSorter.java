package edu.tadzhibaev.taskone.common.utils.sorters;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CollectionSorter {

    private CollectionSorter() {
        //to restrict new instance creation, it's utils class
    }

    public static List<Integer> sortDescent(List<Integer> integers) {
        return integers.stream().sorted(Comparator.naturalOrder()).collect(Collectors.toList());
    }

    public static List<Integer> sortAscent(List<Integer> integers) {
        return integers.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
    }
}
