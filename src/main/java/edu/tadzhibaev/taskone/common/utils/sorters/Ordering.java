package edu.tadzhibaev.taskone.common.utils.sorters;

public enum Ordering {
    NATURAL,
    REVERSED
}
