package edu.tadzhibaev.taskthree.epampages;

import edu.tadzhibaev.AbstractWebPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class EpamJobDetailsPage extends AbstractWebPage {

    EpamJobDetailsPage(WebDriver webDriver) {
        super(webDriver);
    }

    public String getJobDescription() {
        return webDriver.findElement(By.cssSelector("div.recruiting-details-description")).getText().trim();
    }
}
