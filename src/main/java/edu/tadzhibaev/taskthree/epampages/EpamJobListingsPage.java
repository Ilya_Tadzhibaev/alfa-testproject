package edu.tadzhibaev.taskthree.epampages;

import edu.tadzhibaev.AbstractWebPage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EpamJobListingsPage extends AbstractWebPage {

    @FindBy(css = "input.job-search-input")
    private WebElement jobSearchInputField;

    @FindBy(css = "div.career-apply-box-desktop > button.job-search-button")
    private WebElement searchDesktopButton;

    @FindBy(css = "ul.search-result-list")
    private WebElement searchResultList;

    public EpamJobListingsPage(WebDriver webDriver) {
        super(webDriver);
    }

    public EpamJobListingsPage inputTextInJobSearchInputField(String jobTitle) {
        jobSearchInputField.clear();
        jobSearchInputField.sendKeys(jobTitle);
        jobSearchInputField.sendKeys(Keys.ENTER);
        return this;
    }

    public EpamJobListingsPage pressSearchButton() {
        searchDesktopButton.click();
        waitForElementIsVisible(searchResultList);
        return this;
    }

    public EpamJobDetailsPage openLinkFromSearchList(Integer linkPosition) {
        searchResultList.findElement(By.cssSelector(String.format("li:nth-of-type(%d) > div.position-name > a", linkPosition)))
                .click();
        return new EpamJobDetailsPage(webDriver);
    }
}
