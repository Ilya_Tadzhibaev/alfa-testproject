package edu.tadzhibaev.taskthree.epampages;

import edu.tadzhibaev.AbstractWebPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class EpamStartPage extends AbstractWebPage {

    @FindBy(css = "a[href='/careers']")
    private WebElement careers;

    @FindBy(css = "a[href='/careers/job-listings']")
    private WebElement jobListings;


    public EpamStartPage(WebDriver webDriver) {
        super(webDriver);
    }

    public EpamJobListingsPage openCareersPage() {
        openCareersSubMenu();
        waitForElementIsVisible(jobListings);
        jobListings.sendKeys(Keys.ENTER);
        return new EpamJobListingsPage(webDriver);
    }

    private void openCareersSubMenu() {
        Actions actions = new Actions(webDriver);
        actions.moveToElement(careers)
                .build()
                .perform();
    }
}
