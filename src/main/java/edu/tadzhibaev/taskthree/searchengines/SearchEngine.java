package edu.tadzhibaev.taskthree.searchengines;

import java.time.LocalDateTime;

public interface SearchEngine {

    String searchSystemName();

    LocalDateTime getSearchStartDateTime();

}
