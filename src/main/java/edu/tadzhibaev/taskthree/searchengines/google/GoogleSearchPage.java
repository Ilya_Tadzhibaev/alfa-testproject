package edu.tadzhibaev.taskthree.searchengines.google;

import edu.tadzhibaev.AbstractWebPage;
import edu.tadzhibaev.taskthree.searchengines.SearchEngine;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;

public class GoogleSearchPage extends AbstractWebPage implements SearchEngine {

    private static final String GOOGLE_URL = "https://www.google.com";
    @FindBy(css = "input[name='q'][type='text']")
    protected WebElement searchInputField;
    private LocalDateTime searchDateTime;

    public GoogleSearchPage(WebDriver webDriver) {
        super(webDriver);
    }

    public GoogleSearchPage open() {
        webDriver.navigate().to(GOOGLE_URL);
        searchDateTime = LocalDateTime.now();
        return this;
    }

    public GoogleSearchResultsPage searchForText(String text) {
        searchInputField.clear();
        searchInputField.sendKeys(text);
        searchInputField.sendKeys(Keys.ENTER);
        return new GoogleSearchResultsPage(webDriver);

    }

    @Override
    public String searchSystemName() {
        return "Google";
    }

    @Override
    public LocalDateTime getSearchStartDateTime() {
        return searchDateTime;
    }
}
