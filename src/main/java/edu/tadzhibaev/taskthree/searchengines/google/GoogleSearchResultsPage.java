package edu.tadzhibaev.taskthree.searchengines.google;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class GoogleSearchResultsPage extends GoogleSearchPage {

    @FindBy(css = "div.rc > h3 > a")
    private List<WebElement> searchResultLinks;

    public GoogleSearchResultsPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void openSearchResultLink(Integer linkPosition) {
        searchResultLinks.get(linkPosition - 1).sendKeys(Keys.ENTER);
    }
}
