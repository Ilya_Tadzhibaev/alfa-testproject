package edu.tadzhibaev.taskthree.searchengines.yandex;

import edu.tadzhibaev.AbstractWebPage;
import edu.tadzhibaev.taskthree.searchengines.SearchEngine;
import org.openqa.selenium.WebDriver;

import java.time.LocalDateTime;

public class YandexSearchPage extends AbstractWebPage implements SearchEngine {

    public YandexSearchPage(WebDriver webDriver) {
        super(webDriver);
    }

    @Override
    public String searchSystemName() {
        return "Yandex";
    }

    @Override
    public LocalDateTime getSearchStartDateTime() {
        return null;
    }
}
