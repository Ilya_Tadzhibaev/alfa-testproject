package edu.tadzhibaev.taskthree.utils;

import com.google.common.io.Files;
import edu.tadzhibaev.taskthree.searchengines.SearchEngine;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.format.DateTimeFormatter;
import java.util.StringJoiner;

public class JobDescriptionFileCreator {

    private SearchEngine searchEngine;
    private WebDriver webDriver;
    private String descriptionText;

    private String dateTimeFormatterPattern = "yyyy-MM-dd_HH-mm";

    public JobDescriptionFileCreator(SearchEngine searchEngine, WebDriver webDriver, String descriptionText) {
        this.searchEngine = searchEngine;
        this.webDriver = webDriver;
        this.descriptionText = descriptionText;
    }

    public void createInFolder(String folderPath) throws IOException {
        if (folderPath.isEmpty()) {
            throw new RuntimeException("Path to folder can't be empty");
        }

        StringJoiner stringJoiner = new StringJoiner("_")
                .add(searchEngine.getSearchStartDateTime().format(DateTimeFormatter.ofPattern(dateTimeFormatterPattern)))
                .add(searchEngine.searchSystemName())
                .add(webDriver.getClass().getSimpleName())
                .add(".txt");
        File file = new File(folderPath + File.separator + stringJoiner);

        Files.createParentDirs(file);
        Files.write(descriptionText, file, Charset.forName("UTF-8"));
    }
}
