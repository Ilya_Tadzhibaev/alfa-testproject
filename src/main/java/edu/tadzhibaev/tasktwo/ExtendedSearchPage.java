package edu.tadzhibaev.tasktwo;

import edu.tadzhibaev.AbstractWebPage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ExtendedSearchPage extends AbstractWebPage {

    @FindBy(css = "input#glpricefrom")
    private WebElement lowPriceField;

    @FindBy(css = "input#glpriceto")
    private WebElement topPriceField;

    @FindBy(xpath = "//legend[contains(text(),'Производитель')]/following-sibling::ul//input[@type='checkbox']")
    private List<WebElement> labelCheckboxes;

    public ExtendedSearchPage(WebDriver webDriver) {
        super(webDriver);
    }

    public ExtendedSearchPage setLowPrice(Integer lowPrice) {
        lowPriceField.sendKeys(String.valueOf(lowPrice));
        waitForElementIsVisible(By.cssSelector(".spin2_progress_yes"));
        waitForElementDisappears(By.cssSelector(".spin2_progress_yes"));
        return this;
    }

    public ExtendedSearchPage setTopPrice(Integer topPrice) {
        topPriceField.sendKeys(String.valueOf(topPrice));
        waitForElementIsVisible(By.cssSelector(".spin2_progress_yes"));
        waitForElementDisappears(By.cssSelector(".spin2_progress_yes"));
        return this;
    }

    public ExtendedSearchPage tickLabelByNames(String... names) {

        List<String> namesList = Arrays.stream(names).
                map(String::toLowerCase)
                .collect(Collectors.toList());

        for (WebElement labelCheckBox : labelCheckboxes) {
            String label = labelCheckBox.findElement(By.xpath("./following-sibling::div/span")).getText();
            if (namesList.remove(label.toLowerCase())) {
                labelCheckBox.sendKeys(Keys.SPACE);
            }

        }
        return this;
    }

    public String getProductTitle(Integer productPosition) {
        return webDriver.findElement(By.cssSelector(String.format("div.n-snippet-list > div:nth-of-type(%d) div.n-snippet-cell2__title > a", productPosition))).getText();
    }

    public ProductDescriptionPage openProductDescriptionPage(Integer productPosition) {
        webDriver.findElement(By.cssSelector(String.format("div.n-snippet-list > div:nth-of-type(%d) div.n-snippet-cell2__title > a", productPosition))).sendKeys(Keys.ENTER);
        return new ProductDescriptionPage(webDriver);
    }
}
