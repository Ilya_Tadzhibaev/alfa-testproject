package edu.tadzhibaev.tasktwo;

import edu.tadzhibaev.AbstractWebPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage extends AbstractWebPage {

    private final static String MAIN_PAGE_URL = "http://yandex.ru";

    @FindBy(css = "[data-id='market']")
    private WebElement marketLink;

    public MainPage(final WebDriver webDriver) {
        super(webDriver);
        open();
    }

    private void open() {
        webDriver.get(MAIN_PAGE_URL);
    }

    public MarketPage openMarketPage() {
        marketLink.click();
        return new MarketPage(webDriver);
    }
}
