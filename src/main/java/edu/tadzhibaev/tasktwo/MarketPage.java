package edu.tadzhibaev.tasktwo;

import edu.tadzhibaev.AbstractWebPage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class MarketPage extends AbstractWebPage {

    @FindBy(xpath = "//li[@data-department='Электроника']")
    private WebElement electronisMenu;

    public MarketPage(final WebDriver webDriver) {
        super(webDriver);
    }

    private void openDropDownMenu(WebElement webElement) {
        Actions actions = new Actions(webDriver).moveToElement(electronisMenu);
        actions.build().perform();
    }

    public ExtendedSearchPage openMobilePhonesPage() {
        openDropDownMenu(electronisMenu);
        electronisMenu.findElement(By.xpath(".//a[contains(text(),'Мобильные телефоны')]")).sendKeys(Keys.ENTER);
        return new ExtendedSearchPage(webDriver);
    }

    public ExtendedSearchPage openHeadphonesMenu() {
        openDropDownMenu(electronisMenu);
        electronisMenu.findElement(By.xpath(".//a[contains(text(),'Наушники')]")).sendKeys(Keys.ENTER);
        return new ExtendedSearchPage(webDriver);
    }

}
