package edu.tadzhibaev.tasktwo;

import edu.tadzhibaev.AbstractWebPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ProductDescriptionPage extends AbstractWebPage {

    public ProductDescriptionPage(WebDriver webDriver) {
        super(webDriver);
    }

    public String getProductTitleText() {
        return webDriver.findElement(By.cssSelector("h1.title")).getText();
    }


}
