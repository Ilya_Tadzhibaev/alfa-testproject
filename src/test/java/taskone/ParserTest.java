package taskone;

import edu.tadzhibaev.taskone.common.utils.FileUtils;
import edu.tadzhibaev.taskone.common.utils.sorters.Ordering;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class ParserTest {

    private static final String FILE_NAME = "numbers.txt";

    @Test
    public void runParsers() throws IOException, URISyntaxException {
        FileUtils.sortAndPrintResult(FILE_NAME, Ordering.NATURAL);
        FileUtils.sortAndPrintResult(FILE_NAME, Ordering.REVERSED);
    }
}
