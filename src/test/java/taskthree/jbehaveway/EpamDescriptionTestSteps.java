package taskthree.jbehaveway;

import edu.tadzhibaev.taskthree.epampages.EpamJobDetailsPage;
import edu.tadzhibaev.taskthree.epampages.EpamJobListingsPage;
import edu.tadzhibaev.taskthree.epampages.EpamStartPage;
import edu.tadzhibaev.taskthree.searchengines.google.GoogleSearchPage;
import edu.tadzhibaev.taskthree.searchengines.google.GoogleSearchResultsPage;
import edu.tadzhibaev.taskthree.utils.JobDescriptionFileCreator;
import org.jbehave.core.annotations.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class EpamDescriptionTestSteps {

    private WebDriver webDriver;

    private GoogleSearchPage googleSearchPage;
    private GoogleSearchResultsPage resultsPage;

    private EpamStartPage epamStartPage;
    private EpamJobListingsPage epamJobListingsPage;
    private EpamJobDetailsPage epamJobDetailsPage;

    @BeforeScenario
    public void init() {
        System.setProperty("webdriver.chrome.driver", ClassLoader.getSystemResource("chromedriver.exe").getPath());
        webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
    }

    @AfterScenario
    public void tearUp() {
        webDriver.close();
        webDriver.quit();
    }

    @Given("open Chrome browser")
    public void openChromeBrowser() {
        googleSearchPage = new GoogleSearchPage(webDriver);
        googleSearchPage.open();
    }

    @When("open \"$url\" search page")
    public void openPage(String url) {
        webDriver.navigate().to(url);
    }

    @When("search for \"$text\" text")
    public void searchForText(String text) {
        resultsPage = googleSearchPage.searchForText(text);
    }

    @When("open $linkNumberFromTop result link")
    public void openResultLink(Integer linkNumberFromTop) {
        resultsPage.openSearchResultLink(linkNumberFromTop);
    }

    @Then("should be \"$pageTitle\" page title")
    public void shouldBePageTitle(String pageTitle) {
        assertThat(webDriver.getTitle(), equalTo(pageTitle));
    }

    @When("open epam details page")
    public void openEpamDetailsPage() {
        epamStartPage = new EpamStartPage(webDriver);
    }

    @When("open careers page")
    public void openCareersPage() {
        epamJobListingsPage = epamStartPage.openCareersPage();
    }

    @When("input \"$text\" in job title field")
    public void inputTextJobTitleField(String text) {
        epamJobListingsPage.inputTextInJobSearchInputField(text);
    }

    @When("press Search button with results waiting")
    public void pressSearchButton() {
        epamJobListingsPage.pressSearchButton();
    }

    @When("open $linkNumberFromTop result position")
    public void openResultJobPosition(Integer linkNumberFromTop) {
        epamJobDetailsPage = epamJobListingsPage.openLinkFromSearchList(linkNumberFromTop);
    }

    @When("copy text and save it to file in \"$folderPath\" folder")
    public void saveDescriptionToFileInFolder(String folderPath) throws IOException {
        String jobDescription = epamJobDetailsPage.getJobDescription();

        JobDescriptionFileCreator jobDescriptionFileCreator = new JobDescriptionFileCreator(googleSearchPage, webDriver, jobDescription);
        jobDescriptionFileCreator.createInFolder(folderPath);
    }

}
