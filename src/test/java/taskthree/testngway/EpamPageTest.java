package taskthree.testngway;

import edu.tadzhibaev.taskthree.epampages.EpamJobDetailsPage;
import edu.tadzhibaev.taskthree.epampages.EpamStartPage;
import edu.tadzhibaev.taskthree.searchengines.google.GoogleSearchPage;
import edu.tadzhibaev.taskthree.utils.JobDescriptionFileCreator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class EpamPageTest {

    private WebDriver webDriver;

    @BeforeMethod
    public void init() {
        System.setProperty("webdriver.chrome.driver", ClassLoader.getSystemResource("chromedriver.exe").getPath());
        webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
    }

    @AfterMethod
    public void tearDown() {
        webDriver.close();
        webDriver.quit();
    }

    @Test(description = "Navigate to epam's portal, find job, copy job description to file")
    public void navigateToEpamAndCopyText() throws IOException {
        GoogleSearchPage googleSearchPage = new GoogleSearchPage(webDriver);
        googleSearchPage.open()
                .searchForText("Epam group")
                .openSearchResultLink(1);

        assertThat(webDriver.getTitle(), is("EPAM | Разработка ПО"));

        EpamJobDetailsPage epamJobDetailsPage = new EpamStartPage(webDriver)
                .openCareersPage()
                .inputTextInJobSearchInputField("Senior Software Test Automation Engineer")
                .pressSearchButton()
                .openLinkFromSearchList(1);
        String jobDescription = epamJobDetailsPage.getJobDescription();

        JobDescriptionFileCreator jobDescriptionFileCreator = new JobDescriptionFileCreator(googleSearchPage, webDriver, jobDescription);
        jobDescriptionFileCreator.createInFolder("C:\\logs");
    }

}
