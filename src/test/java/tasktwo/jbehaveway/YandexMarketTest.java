package tasktwo.jbehaveway;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

public class YandexMarketTest extends JUnitStories {

    @Test
    public void run() throws Throwable {
        super.run();
    }

    @Override
    protected List<String> storyPaths() {
        return Arrays.asList("tasktwo.stories/yandex_market_test.story");
    }

    @Override
    public Configuration configuration() {
        return new MostUsefulConfiguration();
    }

    @Override
    public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new YandexMarketTestSteps());
    }
}
