package tasktwo.jbehaveway;

import edu.tadzhibaev.tasktwo.ExtendedSearchPage;
import edu.tadzhibaev.tasktwo.MainPage;
import edu.tadzhibaev.tasktwo.MarketPage;
import edu.tadzhibaev.tasktwo.ProductDescriptionPage;
import org.jbehave.core.annotations.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.testng.AssertJUnit.assertEquals;

public class YandexMarketTestSteps {

    private WebDriver webDriver;

    private MarketPage marketPage;
    private ExtendedSearchPage extendedSearchPage;
    private Integer productPosition;
    private String productTitleOnSearchPage;
    private ProductDescriptionPage productDescriptionPage;
    private String descriptionPageProductTitleText;

    @BeforeScenario
    public void init() {
        System.setProperty("webdriver.chrome.driver", ClassLoader.getSystemResource("chromedriver.exe").getPath());
        webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
    }

    @AfterScenario
    public void tearUp() {
        webDriver.close();
        webDriver.quit();
    }

    @Given("open yandex market page")
    public void openYandexMarketPage() {
        MainPage mainPage = new MainPage(webDriver);
        marketPage = mainPage.openMarketPage();
    }

    @When("open mobile phones page")
    public void openMobilePhonesPage() {
        extendedSearchPage = marketPage.openMobilePhonesPage();
    }

    @When("open mobile headphones page")
    public void openHeadphonesPage() {
        extendedSearchPage = marketPage.openHeadphonesMenu();
    }

    @When("tick \"$labels\" lables")
    public void tickLabels(List<String> lables) {
        lables.forEach(extendedSearchPage::tickLabelByNames);
    }

    @When("set low price to $price")
    public void setLowPrice(Integer price) {
        extendedSearchPage.setLowPrice(price);
    }

    @When("set top price to $price")
    public void setTopPrice(Integer price) {
        extendedSearchPage.setTopPrice(price);
    }

    @When("get product title on $priductPosition position in search result")
    public void getProducTitle(Integer productPosition) {
        this.productPosition = productPosition;
        productTitleOnSearchPage = extendedSearchPage.getProductTitle(productPosition);
    }

    @When("open product description page")
    public void openProductDesctiptionPage() {
        productDescriptionPage = extendedSearchPage.openProductDescriptionPage(productPosition);
    }

    @When("get product title text")
    public void getProductTitleOnDescriptionPage() {
        descriptionPageProductTitleText = productDescriptionPage.getProductTitleText();
    }

    @Then("product title on search page equals to product title on  desctiption page")
    public void assertTitles() {
        assertEquals("Product titles doesn't match", productTitleOnSearchPage, descriptionPageProductTitleText);
        System.out.println("Product titles match");
    }

}
