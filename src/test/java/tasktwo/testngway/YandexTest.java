package tasktwo.testngway;

import edu.tadzhibaev.tasktwo.ExtendedSearchPage;
import edu.tadzhibaev.tasktwo.MainPage;
import edu.tadzhibaev.tasktwo.MarketPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


public class YandexTest {

    private ThreadLocal<WebDriver> driverThreadLocal = new ThreadLocal<>();
    private MarketPage marketPage;

    private synchronized WebDriver getDriverThreadLocal() {
        if (null == driverThreadLocal.get()) {
            driverThreadLocal.set(new ChromeDriver());
        }
        return driverThreadLocal.get();
    }

    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", ClassLoader.getSystemResource("chromedriver.exe").getPath());
        getDriverThreadLocal().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        MainPage mainPage = new MainPage(getDriverThreadLocal());
        marketPage = mainPage.openMarketPage();
    }

    @AfterMethod
    public void close() {
        getDriverThreadLocal().manage().deleteAllCookies();
        getDriverThreadLocal().close();
        getDriverThreadLocal().quit();
        driverThreadLocal.remove();
    }

    @Test(description = "Check if mobile phone product title on search page matches title on description page")
    public void mobilePhoneTitleTest() {
        int productPosition = 1;

        ExtendedSearchPage extendedSearchPage = marketPage.openMobilePhonesPage()
                .tickLabelByNames("samsung")
                .setLowPrice(40_000);
        String productTitleOnSearchPage = extendedSearchPage.getProductTitle(productPosition);

        String productTitleOnDescriptionPage = extendedSearchPage
                .openProductDescriptionPage(productPosition)
                .getProductTitleText();

        assertThat(productTitleOnSearchPage, is(productTitleOnDescriptionPage));
    }

    @Test(description = "Check if headphones product title on search page matches title on description page")
    public void headphonesTitleTest() {
        int productPosition = 1;

        ExtendedSearchPage extendedSearchPage = marketPage.openHeadphonesMenu()
                .tickLabelByNames("beats")
                .setLowPrice(17_000)
                .setTopPrice(25_000);
        String productTitleOnSearchPage = extendedSearchPage.getProductTitle(productPosition);

        String productTitleOnDescriptionPage = extendedSearchPage
                .openProductDescriptionPage(productPosition)
                .getProductTitleText();

        assertThat(productTitleOnSearchPage, is(productTitleOnDescriptionPage));
    }

}
