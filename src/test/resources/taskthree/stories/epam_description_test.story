Given open Chrome browser
When open "https://www.google.com" search page
And search for "Epam group" text
And open 1 result link
Then should be "EPAM | Разработка ПО" page title
When open epam details page
And open careers page
And input "Senior Software Test Automation Engineer" in job title field
And press Search button with results waiting
And open 1 result position
And copy text and save it to file in "C:\logs" folder