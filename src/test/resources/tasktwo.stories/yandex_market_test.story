Scenario: search for mobile phones and compare title on description page with title on search page
Given open yandex market page
When open mobile phones page
And tick "samsung" lables
And set low price to 40000
And get product title on 1 position in search result
And open product description page
And get product title text
Then product title on search page equals to product title on  desctiption page

Scenario: search for headphones and compare title on description page with title on search page
Given open yandex market page
When open mobile headphones page
And tick "beats" lables
And set low price to 17000
And set top price to 25000
And get product title on 1 position in search result
And open product description page
And get product title text
Then product title on search page equals to product title on  desctiption page